import React from 'react';
import {render} from 'react-dom';
import {HomeView} from './components/Main';
import './scss/main.scss';

import 'bootstrap';


render(<HomeView />,document.getElementById('app'));
