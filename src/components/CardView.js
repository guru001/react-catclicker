import React,{Component} from 'react';
import PropTypes from 'prop-types';

export class CardView extends Component{
    constructor(props)
    {
       super(props);
       this.state={
          data:props.data
       };
this.CalcAge= this.CalcAge.bind(this);
this.increment=this.increment.bind(this);
    }

    componentWillReceiveProps(props){
        this.setState({
            data:props.data
        });
    }

CalcAge(clicks){
  
  let age= (clicks >=0 && clicks <=5 )?'Infant':
    (clicks >5 && clicks <=12)?'Child':
    (clicks >12 && clicks <=25)?'Young':
    (clicks >25 && clicks <=40)?'Middle-Age':
    (clicks >40 && clicks <=60)?'Old':
    (clicks > 60 )?'Very-Old':'';
return age;
}

increment(e){
   
this.props.clicked();
}

    render(){
        let data = this.state.data;

        return (
            <Card center >
            <CardTitle header={data.CatName} />
            <CardImage top image={data.CatImage} clicked={this.increment}/>
            <CardBody  
            title={`No. of clicks : ${data.CatClicks}`}
            subtle={`Aka ${data.CatNickNames}`}
            />
            <CardFooter footer={`${this.CalcAge(data.CatClicks)}`} />
            </Card>
        );
    }
} 

export const Card = (props) =>
<div className={`card ${ (props.center ? 'text-center':'') }`}>
  {props.children}
</div>

Card.propTypes={
    center:PropTypes.bool
}
Card.defaultProps={
    center:false
}
export const CardTitle = (props) =>
<div className="card-header">
      {props.header}
  </div>

CardTitle.propTypes={
    header:PropTypes.string
}

CardTitle.defaultProps={
    header:'Header'
}

export const CardImage = (props) => 
           <img className={props.top ? 'card-img-top':''}
                src={props.image}
                alt={props.alt} 
                onClick={props.clicked}
                />
  
CardImage.propTypes={
top:PropTypes.bool,
image:PropTypes.string.isRequired,
alt:PropTypes.string
}                

CardImage.defaultProps={
      top:true,
      image:'',
      alt:'card image'
  }

export const CardFooter = (props) =>
<div className="card-footer bg-dark text-white">
{props.footer}
</div>

CardFooter.propTypes={
    footer:PropTypes.string
}

CardFooter.defaultProps={
    footer:'Footer'
}

export const CardBody = (props) => 
<div className="card-body">
    <h5 className="card-title">{props.title}</h5>
    <p className="card-text">{props.subtle}</p>
</div>


CardBody.propTypes={
    title:PropTypes.string,
    subtle:PropTypes.string
}

CardBody.defaultProps={
    title:'Title',
    subtle:'Subtle'
}

export default CardView;

