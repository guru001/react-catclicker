import React,{Component} from 'react';
import Storage from './storage';

export class ListView extends Component{

    constructor(props){
    super(props);
  this.state={
    active:props.active,
    data:props.data
  }
  
  this.setActive= this.setActive.bind(this);
  
  }
  componentWillReceiveProps(props){
    this.setState({
      active:props.active,
      data:props.data
    });
  }
  
  componentWillMount(){
    
  }
  
  setActive(e){
  
  this.setState({
    active:this.state.data[e]
  })

  this.props.onChange(e);

  }
  
  
  render(){
    let data= this.state.data;
    let active = this.state.active;

    return (
      <CatList>
    {data.map( 
      (el,i) =>
        <CatItem key={el.CatName} 
                 index={i}
                 update={ () => this.setActive(i)} 
                 isActive={el.CatName===active.CatName} > 
                 {el.CatName}
                 <CatClicks >  {el.CatClicks} </CatClicks>
        </CatItem>  
              )}
      </CatList>
    );
  }
  
  }
  
  export const CatList = (props) => <ul className="list-group">{props.children}</ul>
  
  export const CatItem = (props) => <li index={props.index} className={props.isActive?'list-group-item d-flex justify-content-between align-items-center active':'list-group-item d-flex justify-content-between align-items-center'} onClick={props.update}> {props.children}  </li>
  
  export const CatClicks = (props) =>  <span className=" badge badge-dark">{props.children}</span>

  export default ListView;