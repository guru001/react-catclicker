import React,{Component} from 'react';
import ListView from './ListView';
import CardView from './CardView';
import FormView from './FormView';
import Storage from './storage';

export class Stage extends Component{

constructor(){
    super();
    this.index=0;
    this.state={
      data:[],
      active:{}
    }
    this.sync= this.sync.bind(this);
    this.update= this.update.bind(this);
    this.increment=this.increment.bind(this);
    this.modify=this.modify.bind(this);
    this.createNew = this.createNew.bind(this);
}

createNew(e)
{
let {CatName,CatNickNames,CatImage} = e;

let newCat = {
  CatName,
  CatClicks:0,
  CatNickNames,
  CatImage,
  CatAge:0
}

let data = this.state.data;
data.push(newCat);

console.log(newCat);

this.setState({
  data
})
}


componentWillMount(){

 // const array = new Storage().init().getData('catdata');
this.firebase = new Storage().fire('Cats');


  this.firebase.on('value', (snapshot) =>{
  let array  = snapshot.val();

  this.setState({
    data:array,
    active:array[this.index]
  });

  });

}

sync()
{

this.firebase.update(this.state.data).then((response) =>{
  if(response){
     console.log('data loaded successfully from firebase cloud');
  } 

}).catch((error)=>{
  if(error)
  {
    console.log('error while fetching...');
  }
});

}


increment(e){
  //console.log(this.index,'index');
  
  let data = this.state.data;
  let active=this.state.active;

  data[this.index].CatClicks++;

  this.setState({
      data
           });
}

modify(val){
console.log(val);
let data= this.state.data;

if(Boolean(val.CatName))
{
  
  data[this.index].CatName =val.CatName;
  this.setState({
  data
  })
}

if(val.CatClicks>=0) 
{

  data[this.index].CatClicks=val.CatClicks;
  this.setState({
    data
  });
  
}

}
update(e){
console.log(e);
this.index=e;
this.setState({
  active:this.state.data[e]
});
}

render(){
  let active = this.state.active;
let data = this.state.data;
    return (
      <div className="row my-2 justify-content-center" > 
      <div className="col-lg-4 my-4 col-md-6 col-sm-12">
         <ListView active={active} data={data} onChange={(e)=>this.update(e)} > </ListView>
      </div>

      <div className="col-lg-4 my-4 col-md-6 col-sm-12">
    <CardView data={active} active={active} clicked={this.increment} />
      </div>
      
      <div className="col-lg-4 my-4 col-md-6 col-sm-12">
      <FormView data={active} modify={(e)=>this.modify(e) } create={ (e)=>this.createNew(e)} sync={this.sync} />
      </div>
    
      </div>

    );
}


}

