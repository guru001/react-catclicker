
import firebase from './firebase';

export class Storage {

constructor(){
    this.storage=null;
    this.data=null;
}

init()
{
    this.storage=window.localStorage;
    return this;
}

fire(reference){
    this.data = firebase.database().ref(reference);
    return this.data;
}

getData(key){
    if(this.storage===null)
    throw new Error(" have'nt initialized yet");

    return JSON.parse(this.storage.getItem(key));
}
}

export {Storage as default};
