
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";
import "firebase/messaging";
import "firebase/functions";


let  config = {
    apiKey: "AIzaSyAeCcZ1HNgajs9KlsRMzOzBBXEU4IAKRUs",
    authDomain: "catclicker-r.firebaseapp.com",
    databaseURL: "https://catclicker-r.firebaseio.com",
    projectId: "catclicker-r",
    storageBucket: "catclicker-r.appspot.com",
    messagingSenderId: "1062009896954"
  };
/*
  let config = {
    apiKey: "AIzaSyBQqxnc9HdiimLQCDbc4nt3bLqqcOFoY20",
    authDomain: "catermans-12cb7.firebaseapp.com",
    databaseURL: "https://catermans-12cb7.firebaseio.com",
    projectId: "catermans-12cb7",
    storageBucket: "catermans-12cb7.appspot.com",
    messagingSenderId: "715695269258"
  };
*/

firebase.initializeApp(config);
  
export default firebase;