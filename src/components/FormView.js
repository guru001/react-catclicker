import  React,{Component} from 'react';
import PropTypes from 'prop-types';



export class FormView extends Component{

constructor(props){
super(props);
this.state={
data:props.data,
catName:props.data.CatName,
catClicks:props.data.CatClicks,
valid:false
};
this.submit=this.submit.bind(this);
this.change=this.change.bind(this);
this.newCat=this.newCat.bind(this);
}

newCat(e){
this.props.create(e);
}

componentWillReceiveProps(props){
this.setState({
  data:props.data,
});
}

submit(e){
  e.preventDefault();
let CatName=this.state.catName;
let CatClicks=this.state.catClicks;

if( this.state.valid )
{
this.props.modify({CatName,CatClicks});
}


}

change(){
this.setState({
  catClicks:Number(this.refs.catclicks.value),
  catName:this.refs.catname.value,
  valid: Boolean(this.refs.catname.value) || this.refs.catclicks.value  >=0 
});
}

render(){ 
   let data= this.state.data;
   let valid = this.state.valid;
    return (
     
<div >
        <button type="button" className="my-3 btn btn-block btn-lg btn-primary" data-toggle="modal" data-target="#CatModal">
         Create New Cat ?
        </button>
        
         <Modal header='New Cat' create={(e) =>this.newCat(e)} />

        <form>
      
      
         <div className="form-group">
    <label htmlFor="catname1">Enter CatName</label>
    <input ref="catname" type="text" onChange={this.change} className="form-control"  id="catname1" aria-describedby="catname" placeholder={data.CatName}/>
    <small id="catname" className="form-text text-muted">make sure you enter valid names</small>
  </div>
  <div className="form-group">
    <label htmlFor="clicks1">No. of clicks</label>
    <input ref="catclicks" onChange={this.change} type="number" className="form-control" id="clicks1" placeholder={data.CatClicks}/>
  </div>
 
  <button type="submit" disabled={!valid} className="btn  btn-primary mx-1" onClick={this.submit}>Submit</button>
  <button type="resery" className="btn btn-secondary">Reset</button>
         </form>
        
         <button type="button" className="my-3 btn btn-block btn-lg btn-success" onClick={this.props.sync}>
         <i className="fas fa-cloud text-white"></i>  Sync with Firebase
        </button>

         </div>
    );
}



}

FormView.propTypes={
 data:PropTypes.object,
  Catname:PropTypes.string,
  CatClicks:PropTypes.bool
}

class Modal extends Component{
  constructor(props)
  {
    super(props);
    this.state={
         CatName:'',
         CatNickName:'',
         CatImage:''
    }
this.update=this.update.bind(this);
this.create=this.create.bind(this);
  }

  update(){

this.setState({
CatName:this.refs.CatName.value,
CatNickName:this.refs.CatNickName.value,
CatImage:this.refs.CatImage.value
});

  }

  create(){
   let CatImage = this.state.CatImage;
   let CatName = this.state.CatName;
   let CatNickNames = this.state.CatNickName;

   this.props.create( { CatImage,CatName,CatNickNames });
  }

render(){
  return (
  <div className="modal fade" id="CatModal" tabIndex="-1" role="dialog" aria-labelledby="CatModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
     <div className="modal-header">
      <h5 className="modal-title text-center" id="CatModalLabel">{this.props.header}</h5>
      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div className="modal-body">
      <form>
        <div className="form-group">
          <label htmlFor="CatName" className="col-form-label">New CatName</label>
          <input ref="CatName" onChange={this.update} type="text" className="form-control" id="CatName" placeholder="Mercury"/>
        </div>
        <div className="form-group">
          <label htmlFor="CatNickName" className="col-form-label">Nickname</label>
          <input ref="CatNickName" onChange={this.update}   type="text" className="form-control" id="CatNickName" placeholder="Max"/>
        </div>
        <div className="form-group">
        <label htmlFor="CatImage" className="col-form-label">Url</label>
        <input ref="CatImage" onChange={this.update}  type="text" className="form-control" id="CatImage" placeholder="https://pinterest.com/boards/mcu/batman.jpg" />
      </div>
      </form>
    </div>
    <div className="modal-footer">
      <button type="button" className="btn btn-primary" onClick={this.create} data-dismiss="modal">Create</button>
      <button type="button" className="btn btn-secondary" data-dismiss="modal" >Close</button>
    </div>
  </div>
</div>
</div>
  );
}


}

Modal.defaultProps={
  header:'Default'
}

export {FormView as default}


