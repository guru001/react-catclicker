import React from 'react';
import {Stage} from './stage';
import data from '../data/catData.json';

window.localStorage.setItem('catdata',JSON.stringify(data));

export class HomeView extends React.Component{
 
    constructor()
    {
        super();
        
    }

    render(){
        return(
            <div className="container">
            <div className="display-3 text-center">
            Welcome to the CatClicker App
            </div>
            <Stage />
            </div>
        );
    }

}